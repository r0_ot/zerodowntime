## Zero Downtime
I will explain how to take the system out of the circuit to update or do other purposes without any disturbance in the anycast technique.

To facilitate understanding of the solution, you can consider the following simple network:

### Topology
![Topology](./images/topology.png)

Firstly, let me describe this network. Suppose you are a client and you want to resolve your website that serves in the server with IP address `10.0.0.10`. When you ask your address
and type that in your browser, after DNS resolvation your system generates a packet and sends it to the network.
Your packet should find its path by crossing through the routers. After a while, your packet received via `BGP` router which should decide on the next router due to its table.
According to this protocol, it chooses the best path (shortest path) for routing. So due to the figure above, router `Router1` will be chosen and the packet enters the `10.0.0.0/24` network and then will be delivered to the `Server1`.

### Consideratoins
We want to take `Server1` out of the circuit, but the important challenge is `ESTABLISHED` connections. what should we do with these connections?
We can wait for `ESTABLISHED` connections to `close`, but what about `NEW` connections?

### Main Problem
Due to the mentioned issue, we need to think about `NEW` connections. At first glance, we are saying: `That's easy. we can forward new connection requests to the new server, right ?`
Yes, it is, but we have an issue. we are using the anycast technique, it means if we want to forward new packets toward the new server we have a destination address that exists in our network, therefore, when we decide to send the packets to a new server, unfortunately, the packet returned to our network.
so what should we do?

### Tunneling
Since we use the anycast technique, if we want to handle a new packet with the same address in our network, we have to send this packet to another network to decide that.
So we make a tunnel between `Firewall1` and `Router1`, tunnel between `Router1` and `Router3` (for instance) and another between `Router3` and `Firewall2`. `GRE` is the best choice, because it is supported by most vendors and also is flexible to encryption with `IPSec` and designed for this purpose.

#### Solution
We should forward new connections through this tunnel from the firewall toward the router and find a new path from that, therefore, you need configure some devices:
- firewall
    - GRE (Between `Firewall1` and `Router1`)
    - add new rules for pass new connections through the GRE interface
 
- Routers
    - GRE (Between `Router1` and `Firewall1`, Between `Router1` and `Router3`, Between `Router3` and `Firewall2`)
    - add a new routing


#### Configurations

In order to implement the solution, the [GNS3](https://gns3.com/) have been used.

##### Routers

- `Router1` Configurations
    ```bash
    interface Loopback0
     ip address 2.2.2.2 255.255.255.255
    !
    interface Tunnel1
     ip address 192.168.30.1 255.255.255.0
     tunnel source FastEthernet0/0
     tunnel destination 192.168.7.2
    !
    interface Tunnel2
     ip address 192.168.31.2 255.255.255.0
     tunnel source FastEthernet1/0
     tunnel destination 10.0.0.254
    !
    interface FastEthernet0/0
     ip address 192.168.5.2 255.255.255.0
     duplex auto
     speed auto
    !
    interface FastEthernet1/0
     ip address 10.0.0.1 255.255.255.0
     duplex auto
     speed auto
    !
    interface FastEthernet2/0
     no ip address
     shutdown
     duplex auto
     speed auto
    !
    interface FastEthernet3/0
     no ip address
     shutdown
     duplex auto
     speed auto
    !
    !
    router ospf 1
     log-adjacency-changes
     redistribute bgp 2 subnets
     network 2.2.2.2 0.0.0.0 area 0
     network 10.0.0.0 0.0.0.255 area 0
     network 192.168.5.0 0.0.0.255 area 0
    !
    router ospf 2
     log-adjacency-changes
     network 192.168.30.0 0.0.0.255 area 1
     network 192.168.31.0 0.0.0.255 area 1
    !
    router bgp 2
     no synchronization
     bgp log-neighbor-changes
     network 2.2.2.2 mask 255.255.255.255
     network 192.168.5.0
     redistribute ospf 1
     neighbor 192.168.5.1 remote-as 1
     no auto-summary
    !
    ```
  
- `Router2` Configurations
    ```bash
    interface Loopback0
    ip address 3.3.3.3 255.255.255.255
    !
    interface FastEthernet0/0
    ip address 192.168.6.2 255.255.255.0
    duplex auto
    speed auto
    !
    interface FastEthernet1/0
    ip address 192.168.7.1 255.255.255.0
    duplex auto
    speed auto
    !
    !
    router ospf 1
    log-adjacency-changes
    redistribute bgp 3 subnets
    network 3.3.3.3 0.0.0.0 area 0
    network 192.168.7.0 0.0.0.255 area 0
    !
    router bgp 3
    no synchronization
    bgp log-neighbor-changes
    network 192.168.6.0
    redistribute ospf 1
    neighbor 192.168.6.1 remote-as 1
    no auto-summary
    !
    ```

- `Router3` Configurations
    ```bash
    interface Loopback0
     ip address 4.4.4.4 255.255.255.255
    !
    interface Tunnel1
     ip address 192.168.30.2 255.255.255.0
     tunnel source FastEthernet0/0
     tunnel destination 192.168.5.2
    !
    interface Tunnel2
     ip address 192.168.32.1 255.255.255.0
     tunnel source FastEthernet1/0
     tunnel destination 10.0.0.254
    !
    interface FastEthernet0/0
     ip address 192.168.7.2 255.255.255.0
     duplex auto
     speed auto
    !
    interface FastEthernet1/0
     ip address 10.0.0.1 255.255.255.0
     duplex auto
     speed auto
    !
    interface FastEthernet2/0
     no ip address
     shutdown
     duplex auto
     speed auto
    !
    interface FastEthernet3/0
     no ip address
     shutdown
     duplex auto
     speed auto
    !
    !
    router ospf 1
     log-adjacency-changes
     network 4.4.4.4 0.0.0.0 area 0
     network 10.0.0.0 0.0.0.255 area 0
     network 192.168.7.0 0.0.0.255 area 0
    !
    router ospf 2
     log-adjacency-changes
     network 192.168.30.0 0.0.0.255 area 1
     network 192.168.32.0 0.0.0.255 area 1
    !
    ```

- `BGP` Configurations
    ```bash
    interface Loopback0
    ip address 1.1.1.1 255.255.255.255
    !
    interface FastEthernet0/0
    ip address 192.168.5.1 255.255.255.0
    duplex auto
    speed auto
    !
    interface FastEthernet1/0
    ip address 192.168.6.1 255.255.255.0
    duplex auto
    speed auto
    !
    interface FastEthernet2/0
    ip address 192.168.20.1 255.255.255.0
    duplex auto
    speed auto
    !
    !
    router bgp 1
    no synchronization
    bgp log-neighbor-changes
    network 1.1.1.1 mask 255.255.255.255
    network 192.168.5.0
    network 192.168.6.0
    network 192.168.20.0
    neighbor 192.168.5.2 remote-as 2
    neighbor 192.168.6.2 remote-as 3
    no auto-summary
    !
    ```

###### _Test Configurations_

Now, it's time to test our configurations

- Table of `BGP`

    ![BGP Table](./images/bgptable.png)

- Ping `Router1` from `Router3` 

    ![Ping Router1 from Router3](./images/r1r3ping.png)

- Ping `Router3` from `BGP`

    ![Ping Router1 from BGP](./images/r3bgpping.png)


##### Firewalls
we need to configure the firewalls in both sides.

_Most of the following instructions are same in both firewalls_

1. we need to define Virtual IP(s)

    ![Virtual IP](./images/virtualip.png)

2. now we define `1:1 NAT` for our internal services:

    ![1:1 NAT](./images/oonat.png)

3. define rules to pass the traffic from the firewall
    1. Pass `WAN` into firewall

        ![WAN Rules](./images/wan_rules.png)

    2. Pass `WAN` to `LAN`

        ![LAN Rules](./images/lan_rules.png)
        
    3. Pass `OPT1` (Tunnel) traffic
    
        ![GRE Rules](./images/opt1_rules.png)
        
    4. we also need to define new Rules in `Firewall1` for `NAT`
    
        ![NAT Rules](./images/nat_rules_f1.png)
        
        *_we don't change default rules of `NAT` in `Firewall2`_
        
    5. 

4. In order to Routing in the firewalls we need to download `Quagga OSPFd` package and configure that:
    
    - `Firewall1`
        1. Assign Interfaces
            
            ![OSPFd Interfaces](./images/ospfd_f1_interfaces.png)
    
        2. Configurations
        
            ![OSPFd Confiuration](./images/ospfd_f1_config.png)

    - `Firewall2`
        1. Assign Interfaces
            
            ![OSPFd Interfaces](./images/ospfd_f2_interfaces.png)
    
        2. Configurations
        
            ![OSPFd Confiuration](./images/ospfd_f2_config.png)
 


###### _Test Configurations_

- Ping `10.0.0.10` from Client

    ![Ping 10.0.0.10](./images/ping.png)

- Traceroute `10.0.0.10` from Client
   
    ![Traceroute 10.0.0.10](./images/traceroute.png)

- Tunneling
    
    - Between `Firewall1` and `Firewall2`
    
        ![Traceroute 10.0.0.10](./images/traceroute_f1_f2.png)
    
    - Testing `Service` from `Client`
        
        - Traceroute before apply the solution
        
            ![Traceroute 10.0.0.10](./images/traceroute_server1.png)
        
        - Traceroute after apply the solution
            
            ![Traceroute 10.0.0.10](./images/traceroute_server2.png)
            
    - Testing `High Availability (HA)`
        
        - Before apply the solution
            
            ![Before apply the solution](./images/test_server1.png)
        
        - After change the internal IP (`ESTABLISHED` connections)
            
            ![Before apply the solution](./images/test_server1_established_connections.png)
        
        - After change the internal IP (`NEW` connections)
            
            ![Before apply the solution](./images/test_server2_new_connections.png)

#### References
- [Anycast - Cloudflare](https://www.cloudflare.com/learning/cdn/glossary/anycast-network/)
- [Magic Transit - Cloudflare](https://blog.cloudflare.com/magic-transit-network-functions/)
- [GRE Cisco - Imperva](https://docs.imperva.com/bundle/cloud-application-security/page/onboarding/gre-cisco.htm)
- [GRE Client - Imperva](https://docs.imperva.com/bundle/cloud-application-security/page/onboarding/gre-ubuntu.htm)